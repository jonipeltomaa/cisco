# Perus konfiguraation Ciscon Reititimille ja kytkimille
## Reititimen ja kytkimen tyhjentäminen
Reititimen tyhjentäminen onnistuu seuraavilla komennoilla.
```
router>enable
```
Jonka jälkeen reititin voi pyytää laitteen enable salasanaa jos sellainen on laitteeseen asetettu.
```
router#erase startup-config
```
Erase startup-config poistaa reititimestä tallentamattomat configuraatio tiedostot. <br>
Seuraavalla Komennolla käynistetään reititn uudelleen.
```
router#reload
```
## Peruskonfiguraation tekeminen reititimelle ja kytkimelle
Komennolla enable siirrytään reititmen enable tilaan
```
router>enable
```
komennolla configure terminal siirrytään reititmen konfiguraatio tilaan, jossa voi daan tehdä laitteen konfiguroinnit.
```
router#configure terminal
```
Seuraavalla komennolla annetaan reititimelle nimi jolla helpotetaan tunnistamaan laitteen  verkko kaaviosta.
```
router(config)#hostname R1
```
seuraavalla komennolla otetaan käyttöön 0-alivekko
```
R1(config)#ip subnet-zero
```
Seuraavalla komennolla estetään verkosta tulevien domain etsinnät.
```
R1(config)#no ip domain-lookup
```
Seuraavalla komennolla asetetaan enable tilan käyttämä salasana class.
```
R(config)1#enable secret class
```
Seuraavalla komennolla -
```
R1(config)#line console 0
```
Seuraavalla komennolla asetetaan kosoli tilan salasana cisco
```
R1(config)#password cisco
R1#login
```
Seuraavalla komennolla siirrytään virtuaalilitäntöjen käyttöön ottoon
```
R1(config-line)#line vty 0 4
R1(config-line)#password cisco
R1(config-line)#login
R1(config-line)#exit
```
Seuraavalla komennolla mahdollistetaan http serverin käyttö oton.
```
R1(config)#ip http server
```
## Reititimen ja kytkimien liitäntöjen perus konfigurointi
Seuraavalla komennolla konfiguroimmen ethernet liitännän käytön
```
R1(config)#interface ethernet 0
```
Seuraavalla komennolla annetaan ethernet 0 portille ip osoitteen
```
R1(config)#ip address 192.168.1.1 255.255.255.0
```
Seuraavalla komennolla annetaan ethernet 0 portille käskyn ettei ethernet 0 portti ei voida alas ajaa.
```
R1(config)#no shutdown
```
Seuraavalla komennolla konfiguroimme sarjaliitäntä porttia.
```
R1(config)#interface serial 0
```
Seuraavalla komennoilla asetetetaan sarjalitäntä 0 porttiin ip osite.
```
R1(config)#ip address192.168.2.1 255.255.255.0
```
Seuraavalla komennolla asetetaan sarjaliitäntään kello taajuus joka määrää sarjaliitäntä kaapeli kummassa päässä on dce pää.
```
R1(config)#clockrate 64000
```
Seuraava komento estää sarjaliitäntä 0 portin alas ajamisen.
```
R1(config)#no shutdown
```